# <%= appTplName %>

This project is generated with [yo generator-ng-panes](https://panes.im/page2/generator-ng-panes)
version <%= pkg.version %>.

## Build & development

Run `gulp` for building and `gulp serve` for preview.

## Testing

Running `gulp test` will run the unit tests with karma.

## Need help?

Visit [panes.js](http://panesjs.com)


