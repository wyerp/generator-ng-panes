change log

2015-10-01 1.0.0-alpha.1 integration between ng-panes and panes.js generator 
2015-09-21 1.0.0-alpha add back the karma sub generator as deps. The conflict will always show.
2015-09-19 0.9.15 [BREAKING CHANGE] add check remote version, add new project files style (auto upgrade), add new flat --projects to manage your existing projects.
2015-08-30 0.9.4 Save and retrieve project / users settings to recreate same project.
2015-08-29 0.8.6 Beta feature completed
2015-08-24 0.6.7 Alpha version start
2015-08-21 0.1.6 See git log for more details. The generator reach a stable phrase.
2015-08-20 0.0.2 Updateing all the Javascript Angular files template
2015-08-19 0.0.1 Setting up the base structure based on the origianl yeoman-generator-angular
