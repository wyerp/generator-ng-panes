/* global angular , document */
'use strict';

/**
 * @ngdoc overview
 * @name <%= scriptAppName %>
 * @description
 * # <%= scriptAppName %>
 *
 * Main module of the application.
 */
angular.module('<%= scriptAppName %>', [<%- angularModules %>])
<% if (ngRoute) { %>
.config(['$routeProvider' , function ($routeProvider)
{
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
    })
    .otherwise({
        redirectTo: '/'
    });

}])<% } %>;
//Then init the app
angular.element(document).ready(function()
{
	angular.bootstrap(document, ['<%= scriptAppName %>']);
});

// also provide a appController here, althought its not recommended to put anything in the $rootScope
/*
angular.module('<%= scriptAppName %>').run(['$rootScope' ,'$window' , function($rootScope , $window)
{
    // do your thing here
}]);
*/
